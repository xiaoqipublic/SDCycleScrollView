import XCTest
@testable import SDCycleScrollView

final class SDCycleScrollViewTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SDCycleScrollView().text, "Hello, World!")
    }
}
